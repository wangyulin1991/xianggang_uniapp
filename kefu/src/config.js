// 开发环境域名

const host_development = 'https://shopq43z12ci.yixiangonline.com'

export default {
    // 版本
    version: '1.4.0',
    baseURL: process.env.NODE_ENV == 'production' ? '' : host_development,
    ChatWss: 'wss://duokiachat.yixiangonline.com'
}


