import Cookies from 'js-cookie'
export default ({ isHMR, app, store,query, error }) => {
  if ( isHMR ) return  //是否是通过模块热替换 webpack hot module replacement (仅在客户端以 dev 模式)
   
   let localeNow = ''
   let localeCookie = Cookies.get('lang')
   let localeReq = query.lang //
   let localeSystem = app.i18n.fallbackLocale //系统默认设置
   if(localeCookie != "" && localeCookie != undefined){
       localeNow = localeCookie
   }

   if(localeNow == "" && localeReq != undefined && store.state.locales.includes(localeReq)){
        Cookies.set('lang',localeReq)
        localeNow = localeReq 
   }

   if(localeCookie != "" && localeReq != "" && localeReq != undefined && localeCookie != "" && localeCookie != localeReq){
        Cookies.set('lang',localeReq)
        localeNow = localeReq 
   }
   
   if(localeNow == ""){
       localeNow = localeSystem
   }

  if (!store.state.locales.includes(localeNow)) {
      localeNow = localeSystem
  }
   store.commit('setLang', localeNow); 
   app.i18n.locale = localeNow
}