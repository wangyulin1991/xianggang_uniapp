import Vue from "vue"
import VueI18n from "vue-i18n"
Vue.use(VueI18n)
export default ({ app,store}) => {
  app.i18n = new VueI18n({
    locale: store.state.locale,  //通过cookie设置国际化（用户选择一次终身不切换）
    fallbackLocale: 'en',   //默认国际化
    messages: {
      en: require('~/locales/en-US.json'), //英文
      zh: require('~/locales/zh-CN.json') //中文
    }
  })
}