
// 本地访问域名
// const testUrl = "https://likeshopb2b2c.yixiangonline.com"
const testUrl = "http://shop.b2cplusduokai.dev"

//线上域名
const productUrl = ''
const config = {
  baseUrl:  process.env.NODE_ENV == 'production' ? productUrl : testUrl
}

// export default config

module.exports = {
	version: '1.4.0',			// 版本号
	config,
}
