
// Root
export interface RootState {
    [prop: string]: any
    version: string;
    coder: string;
}

// APP
export interface AppState {
    permission: any
    config: any
}

// User
export interface UserState {
    userInfo: {
        readonly avatar: string;
        readonly name: string;
        readonly role_name: string;
        readonly token: string;
    } | any
}
