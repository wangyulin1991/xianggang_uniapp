const path = require('path')
module.exports = {
  publicPath: '/platform/',
  lintOnSave: false,
  css: {
    loaderOptions: {
      sass: {
        prependData: `@import "@/styles/variables.scss";`
      }
    }
  },
  transpileDependencies: [
    'resize-detector'
  ],
}