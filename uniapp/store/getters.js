export default {
	// 公共配置
	appConfig: state => state.app.config || {},
	// 用户信息
	userInfo: state => state.app.userInfo || {},
	// token
	token: state => state.app.token,
	// 客户端
	client: state => state.app.client,
	// 是否登录
	isLogin: state => !!state.app.token,
	// 主题名字
	themeName: state => state.decorate.config.theme || 'red_theme',
	// 主题颜色
	themeColor: state  => {
		const {
			theme,
			config
		} = state.decorate
		return theme[config.theme] || '#FF2C3C'
	},
	// 底部导航
	tabbar:  state => state.decorate.config.tabbar || {}
};
