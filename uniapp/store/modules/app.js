import {
	apiConfig
} from '@/api/app'
import {
	apiUserCentre,
	apiDistributionCode
} from '@/api/user'
import {
	CONFIG,
	USER_INFO,
	TOKEN,
	INVITE_CODE
} from '@/config/cachekey';
import wechath5 from '@/utils/wechath5'
import Cache from '@/utils/cache'
import {router} from '@/router'
import { getClient } from '@/utils/tools'
const state = {
	config: Cache.get(CONFIG) || {},
	userInfo: {},
	token: Cache.get(TOKEN) || null,
	client: getClient() || null,
};

const mutations = {
	setConfig(state, data) {
		state.config = data
		Cache.set(CONFIG, data);
	},
	login(state, data) {
		state.token = data.token;
		Cache.set(TOKEN, data.token);
		// 登录完成绑定邀请码
		const code = Cache.get(INVITE_CODE)
		if (code) {
			apiDistributionCode({
				code,
				hide: 1
			}).finally(() => {
				Cache.remove(INVITE_CODE)
			})
		}
	},
	logout(state) {
		state.token = '';
		state.userInfo = {}
		Cache.remove(TOKEN);
	},
	setUserInfo(state, data) {
		state.userInfo = data
	}
};

const actions = {
	getConfig({
		state,
		commit
	}) {
		return apiConfig().then(res => {
			commit('setConfig', res)
			return Promise.resolve(res)
		}).catch(() => {
			return Promise.reject()
		})
	},
	getUser({
		state,
		commit
	}) {
		return new Promise((resolve, reject) => {
			apiUserCentre().then(res => {
				commit('setUserInfo', res)
				resolve(res)
			}).catch(() => {
				reject()
			})
		})
	},
	setWxShare({
		state
	}, opt) {
		// #ifdef H5
		const {
			share_image,
			share_intro,
			share_title
		} = state.config
		const inviteCode = state.userInfo.code
		const href = window.location.href
		const sym = href.includes('?') ? '&' : '?'
		const option = {
			shareTitle: share_title,
			shareLink: inviteCode ? `${href}${sym}invite_code=${inviteCode}` : href,
			shareImage: share_image,
			shareDesc: share_intro
		}
		wechath5.share(Object.assign(option, opt))
		// #endif
	}

};

export default {
	state,
	mutations,
	actions
};
