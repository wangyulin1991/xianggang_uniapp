import app from "./app";
import decorate from "./decorate";
export default {
	app,
	decorate
};
