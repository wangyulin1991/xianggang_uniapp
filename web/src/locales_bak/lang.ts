const langOptions = [

  {
    lanCode: "en",
    lanName: "English",
  },
  {
    lanCode: "cn",
    lanName: "简体中文",
  },
];
export default langOptions;