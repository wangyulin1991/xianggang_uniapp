import Vue from 'vue'
import Element from 'element-ui'
import i18n from './i18n'

Vue.use(Element,{
  size: "mini",
  i18n:(key:any, value:any) => i18n.t(key, value)
})

