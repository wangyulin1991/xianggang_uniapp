'use strict'
import Vue from 'vue'
import VueI18n from "vue-i18n";
import en from "@/locales/en-us";
import cn from "@/locales/zh-cn";
import cache from '@/utils/cache';
import enLocale from 'element-ui/lib/locale/lang/en'
import zhLocale from 'element-ui/lib/locale/lang/zh-CN'
Vue.use(VueI18n);
const messages : any = {
  en: {
    ...en,
    ...enLocale // 或者用 Object.assign({ message: 'hello' }, enLocale)
  },
  cn: {
    ...cn,
    ...zhLocale // 或者用 Object.assign({ message: '你好' }, zhLocale)
  }
};
const i18n = new VueI18n({
    locale: cache.get("lang") || "cn",
    messages
})

export default i18n