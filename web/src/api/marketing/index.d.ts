
/** S 足迹气泡 **/
export interface FootprintList_Res {
    config: {
        footprint_duration: number, // 时长
        footprint_status: number,   // 状态【0: 禁用 | 1: 启用】
    },
    lists: [{
        id: number,         // ID
        name: string,       // 名称
        type: number,       // 类型
        template: string,   // 模版
        status: number,     // 状态【0: 禁用 | 1: 启用】
    }]
}

export interface FootprintDetail_Req {
    id: number              // 气泡ID
}

export interface FootprintDetail_Res {
    id: number,             // ID
    name: string,           // 名称
    type: number,           // 类型
    template: string,       // 模版
    status: number,         // 状态【0: 禁用 | 1: 启用】
}

export interface FootprintEdit_Req {
    id: number,             // 气泡ID
    status: number,         // 状态【0: 禁用 | 1: 启用】
}

export interface FootprintSetting_Req {
    duration: number,       // 足迹气泡时长
    status: number,         // 状态【0: 禁用 | 1: 启用】
}

/** E 足迹气泡 **/
