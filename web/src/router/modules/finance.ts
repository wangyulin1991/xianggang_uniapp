
import Main from '@/layout/main.vue'
import i18n  from '@/plugins/i18n'
const routes = [{
    path: '/finance',
    name: 'finance',
    meta: {  title: i18n.t("menue.finance.finance")},
    redirect: '/finance/profile',
    component: Main,
    children: [{
        path: '/finance/profile',
        name: 'profile',
        meta: {
            title: i18n.t("menue.finance.finance"),
            parentPath: '/finance',
            icon: 'icon_caiwu',
            permission: ['view']
        },
        component: () => import('@/views/finance/profile.vue')
    }, {
        path: '/finance/user_withdrawal',
        name: 'user_withdrawal',
        meta: {
            title: i18n.t("menue.finance.withdrawCash"),
            parentPath: '/finance',
            icon: 'icon_caiwu_tixian',
            permission: ['view']
        },
        component: () => import('@/views/finance/user_withdrawal.vue')
    }, {
        path: '/finance/account_log',
        name: 'account_log',
        meta: {
            title:  i18n.t("menue.finance.balance"),
            parentPath: '/finance',
            icon: 'icon_caiwu_yue',
            permission: ['view']
        },
        component: () => import('@/views/finance/account_log.vue')
    }, {
		path: '/finance/integral_list',
		name: 'integral_list',
		meta: {
			title: i18n.t("menue.finance.score"),
			parentPath: '/finance',
			icon: 'icon_caiwu_jifen',
            permission: ['view']
		},
		component: () => import('@/views/finance/integral_list.vue')
		
	}, {
        path: '/finance/commission_log',
        name: 'commission_log',
        meta: {
            title: i18n.t("menue.finance.brokerage"),
            parentPath: '/finance',
            icon: 'icon_set_jiaoyi',
            permission: ['view']
        },
        component: () => import('@/views/finance/commission_log.vue')
    }]
}]

export default routes