import Main from '@/layout/main.vue'
import Blank from '@/layout/blank.vue'
import i18n  from '@/plugins/i18n'
const routes = [
    {
        path: '/data',
        name: 'data',
        meta: { title: i18n.t("menue.data.data") },
        redirect: '/data/flow_analysis',
        component: Main,
        children: [{
            path: '/data/flow_analysis',
            name: 'flow_analysis',
            meta: {
                title: i18n.t("menue.data.flowAnalysis"),//flowAnalysis:'流量分析',
                parentPath: '/data',
                icon: 'icon_shuju_liuliang',
                permission: ['view']
            },
            component: () => import('@/views/data/flow_analysis.vue'),
        }, {
            path: '/data/user',
            name: 'user',
            meta: {
                title: i18n.t("menue.data.userAnalysis"), //userAnalysis:'用户分析',
                parentPath: '/data',
                icon: 'icon_shuju',
                permission: ['view']
            },
            component: () => import('@/views/data/user.vue')
        }, {
            path: '/data/transaction',
            name: 'transaction',
            meta: {
                title: i18n.t("menue.data.TransactionAnalysis"),//TransactionAnalysis:'交易分析',
                parentPath: '/data',
                icon: 'icon_dianpu_shoppingCar',
                permission: ['view']
            },
            component: () => import('@/views/data/transaction.vue')
        }, {
            path: '/data/goods/goods',
            name: 'goods',
            meta: {
                title: i18n.t("menue.data.goods"),
                parentPath: '/data',
                icon: 'tradingdata',
                permission: ['view']
            },
            component: Blank,
            redirect: '/data/goods/goods',
            children: [{
                path: '/data/goods/goods',
                name: 'goods',
                meta: {
                    title:i18n.t("menue.data.goods"),
                    parentPath: '/data',
                    permission: ['view']
                },
                component: () => import('@/views/data/goods/goods.vue')
            }, {
                path: '/data/goods/ranking',
                name: 'goods',
                meta: {
                    title: i18n.t("menue.data.Ranking"),
                    parentPath: '/data',
                    permission: ['view']
                },
                component: () => import('@/views/data/goods/ranking.vue')
            }]
        }
        ]
    }
]

export default routes