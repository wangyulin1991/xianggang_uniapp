
import Main from '@/layout/main.vue'
import Blank from '@/layout/blank.vue'
import i18n  from '@/plugins/i18n'
const routes = [{
    path: '/goods',
    name: 'goods',
    meta: { title: i18n.t("menue.goods.goods") },
    redirect: '/goods/lists',
    component: Main,
    children: [
        {
            path: '/goods/lists',
            name: 'goods_lists',
            meta: {
                title: i18n.t("menue.goods.goods"),
                parentPath: '/goods',
                icon: 'icon_goods',
                permission: ['view']
            },
            component: () => import('@/views/goods/lists.vue')
        }, {
            path: '/goods/category',
            name: 'goods_category',
            meta: {
                title: i18n.t("menue.goods.category"),
                parentPath: '/goods',
                icon: 'icon_sort',
                permission: ['view']
            },
            component: () => import('@/views/goods/category.vue')
        }, {
            path: '/goods/brand',
            name: 'goods_brand',
            meta: {
                title: i18n.t("menue.goods.brand"),
                parentPath: '/goods',
                icon: 'icon_pinpai',
                permission: ['view']
            },
            component: () => import('@/views/goods/brand.vue')
        }, {
            path: '/goods/unit',
            name: 'goods_unit',
            meta: {
                title:  i18n.t("menue.goods.goodsUnit"),
                icon: 'icon_danwei',
                parentPath: '/goods',
                permission: ['view']
            },
            component: () => import('@/views/goods/unit.vue')
        }, {
            path: '/goods/supplier',
            name: 'goods_supplier',
            meta: {
                title: i18n.t("menue.goods.supplier"),
                parentPath: '/goods',
                icon: 'icon_gongyingshang',
            },
            redirect: '/goods/supplier/lists',
            component: Blank,
            children: [
                {
                    path: '/goods/supplier/lists',
                    name: 'supplier_lists',
                    meta: {
                        title: i18n.t("menue.goods.supplier"),
                        parentPath: '/goods',
                        permission: ['view']
                    },
                    component: () => import('@/views/goods/supplier/lists.vue'),
                },
                {
                    path: '/goods/supplier/category',
                    name: 'supplier_category',
                    meta: {
                        title: i18n.t("menue.goods.supplierCategory"),
                        parentPath: '/goods',
                        permission: ['view']
                    },
                    component: () => import('@/views/goods/supplier/category.vue'),
                },
                {
                    path: '/goods/supplier/edit',
                    name: 'supplier_edit',
                    meta: {
                        hidden: true,
                        title: '新增供应商',
                        parentPath: '/goods',
                        prevPath: '/goods/supplier/lists'
                    },
                    component: () => import('@/views/goods/supplier/edit.vue'),
                }
            ]
        }, {
            path: '/goods/release',
            name: 'goods_release',
            meta: {
                hidden: true,
                title: '新增商品',
                parentPath: '/goods',
                prevPath: '/goods/lists',
            },
            component: () => import('@/views/goods/release.vue')
        }, {
            path: '/goods/brand_edit',
            name: 'brand_edit',
            meta: {
                hidden: true,
                title: '新增品牌',
                parentPath: '/goods',
                prevPath: '/goods/brand',
            },
            component: () => import('@/views/goods/brand_edit.vue')
        }, {
            path: '/goods/category_edit',
            name: 'category_edit',
            meta: {
                hidden: true,
                title: '新增分类',
                parentPath: '/goods',
                prevPath: '/goods/category',
            },
            component: () => import('@/views/goods/category_edit.vue')
        }, {
            path: '/goods/evaluation',
            name: 'goods_evaluation',
            meta: {
                title:  i18n.t("menue.goods.evaluation"),
                parentPath: '/goods',
                icon: 'icon_pingjia',
                permission: ['view']
            },
            component: () => import('@/views/goods/evaluation.vue')
        },
    ]
}]

export default routes