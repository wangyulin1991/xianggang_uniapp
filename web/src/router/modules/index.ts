
import Main from '@/layout/main.vue'
import i18n  from '@/plugins/i18n'
const routes = [{
    path: '/',
    name: 'index',
    meta: { title: i18n.t("menue.index") },
    redirect: '/index',
    component: Main,
    children: [{
        path: '/index',
        name: 'index',
        meta: {
            hidden: true,
            title: i18n.t("menue.index"),
            parentPath: '/',
            permission: ['view']
        },
        component: () => import('@/views/index/home.vue'),
    },
    ],
}]

export default routes