
import Main from '@/layout/main.vue'
import Blank from '@/layout/blank.vue'
import i18n  from '@/plugins/i18n'
const routes = [
    {
        path: '/marketing',
        name: 'marketing',
        meta: { title: i18n.t("menue.marketing.marketing")  },
        redirect: '/marketing/index',
        component: Main,
        children: [
            {
                path: '/marketing/index',
                name: 'marketing_index',
                meta: {
                    title: i18n.t("menue.marketing.marketing"),
                    parentPath: '/marketing',
                    icon: 'icon_yingxiaowf',
                    permission: ['view']
                },
                component: () => import('@/views/marketing/index.vue')
            }, {
                path: '/marketing/app',
                name: 'marketing_app',
                meta: {
                    title: i18n.t("menue.marketing.application"),
                    parentPath: '/marketing',
                    icon: 'icon_yingyongcenter',
                    permission: ['view']
                },
                component: () => import('@/views/marketing/app.vue')
            }
        ]
    },

    // 优惠券
    {
        path: '/coupon',
        name: 'coupon',
        meta: {
            title: '优惠券',
            hidden: true,
            moduleName: 'coupon'
        },
        redirect: '/coupon/survey',
        component: Main,
        children: [
            {
                path: '/coupon/survey',
                name: 'coupon_survey',
                meta: {
                    hidden: true,
                    title: '优惠券概览',
                    parentPath: '/marketing',
                    icon: 'icon_coupons_data',
                    moduleName: 'coupon',
                    permission: ['view']
                },
                component: () => import('@/views/marketing/coupon/survey.vue')
            }, {
                path: '/coupon/lists',
                name: 'coupon_lists',
                meta: {
                    title: '优惠券',
                    parentPath: '/marketing',
                    icon: 'icon_coupons',
                    moduleName: 'coupon',
                    permission: ['view']
                },
                component: () => import('@/views/marketing/coupon/lists.vue')
            }, {
                path: '/coupon/receive_record',
                name: 'receive_record',
                meta: {
                    title: '领取记录',
                    parentPath: '/marketing',
                    icon: 'icon_order_guanli',
                    moduleName: 'coupon',
                    permission: ['view']
                },
                component: () => import('@/views/marketing/coupon/receive_record.vue')
            }, {
                path: '/coupon/edit',
                name: 'coupon_edit',
                meta: {
                    hidden: true,
                    title: '优惠券',
                    parentPath: '/marketing',
                    prevPath: '/coupon/lists',
                    moduleName: 'coupon'
                },
                component: () => import('@/views/marketing/coupon/edit.vue')
            }, {
                path: '/coupon/grant',
                name: 'coupon_grant',
                meta: {
                    hidden: true,
                    title: '发放优惠券',
                    parentPath: '/marketing',
                    prevPath: '/coupon/lists',
                    moduleName: 'coupon'
                },
                component: () => import('@/views/marketing/coupon/grant.vue')
            },
        ]
    },

    // 分销
    {
        path: '/distribution',
        name: 'distribution',
        meta: {
            title: '分销',
            hidden: true,
            moduleName: 'distribution'
        },
        redirect: '/distribution/survey',
        component: Main,
        children: [{
            path: '/distribution/survey',
            name: 'survey',
            meta: {
                title: '分销概览',
                parentPath: '/marketing',
                moduleName: 'distribution',
                icon: 'icon_fenxiao_data',
                permission: ['view']
            },
            component: () => import('@/views/marketing/distribution/survey.vue')
        }, {
            path: '/distribution/store',
            name: 'store',
            meta: {
                title: '分销商',
                parentPath: '/marketing',
                moduleName: 'distribution',
                icon: 'icon_fenxiao_member',
                permission: ['view']
            },
            children: [
                {
                    path: '/distribution/store',
                    name: 'distribution_store',
                    meta: {
                        title: '分销商',
                        parentPath: '/marketing',
                        moduleName: 'distribution',
                        permission: ['view']
                    },
                    component: () => import('@/views/marketing/distribution/store/store.vue')
                }, {
                    path: '/distribution/store_detail',
                    name: 'distribution_store_detail',
                    meta: {
                        hidden: true,
                        title: '分销商详情',
                        parentPath: '/marketing',
                        prevPath: '/distribution/store',
                        moduleName: 'distribution',
                    },
                    component: () => import('@/views/marketing/distribution/store/store_detail.vue')
                }, {
                    path: '/distribution/distribution_apply',
                    name: 'distribution_apply',
                    meta: {
                        title: '分销申请',
                        parentPath: '/marketing',
                        prevPath: '/distribution/distribution_apply',
                        moduleName: 'distribution',
                        permission: ['view']
                    },
                    component: () => import('@/views/marketing/distribution/store/apply.vue')
                }, {
                    path: '/distribution/distribution_apply_details',
                    name: 'distribution_apply_details',
                    meta: {
                        hidden: true,
                        title: '分销申请详情',
                        parentPath: '/marketing',
                        prevPath: '/distribution/distribution_apply',
                        moduleName: 'distribution',
                    },
                    component: () => import('@/views/marketing/distribution/store/apply_details.vue')
                }, {
                    path: '/distribution/lower_level_list',
                    name: 'lower_level_list',
                    meta: {
                        hidden: true,
                        title: '下级列表',
                        parentPath: '/marketing',
                        prevPath: '/distribution/store',
                        moduleName: 'distribution',
                    },
                    component: () => import('@/views/marketing/distribution/store/lower_level_list.vue')
                }
            ],
            component: Blank
        }, {
            path: '/distribution/distribution_goods',
            name: 'distribution_goods',
            meta: {
                title: '分销商品',
                parentPath: '/marketing',
                icon: 'icon_fenxiao_goods',
                moduleName: 'distribution',
                permission: ['view']
            },
            component: () => import('@/views/marketing/distribution/distribution_goods.vue')
        }, {
            path: '/distribution/distribution_goods_edit',
            name: 'distribution_goods_edit',
            meta: {
                hidden: true,
                title: '设置分销佣金',
                parentPath: '/marketing',
                prevPath: '/distribution/distribution_goods',
                moduleName: 'distribution'
            },
            component: () => import('@/views/marketing/distribution/distribution_goods_edit.vue')
        }, {
            path: '/distribution/distribution_orders',
            name: 'distribution_orders',
            meta: {
                title: '分销订单',
                parentPath: '/marketing',
                icon: 'icon_dianpu_xiangqing',
                moduleName: 'distribution',
                permission: ['view']
            },
            component: () => import('@/views/marketing/distribution/distribution_orders.vue')
        }, {
            path: '/distribution/distribution_grade',
            name: 'distribution_grade',
            meta: {
                title: '分销等级',
                parentPath: '/marketing',
                icon: 'icon_fenxiao_grade',
                moduleName: 'distribution',
                permission: ['view']
            },
            component: () => import('@/views/marketing/distribution/distribution_grade.vue')
        }, {
            path: '/distribution/distribution_grade_edit',
            name: 'distribution_grade_edit',
            meta: {
                hidden: true,
                title: '分销等级编辑',
                parentPath: '/marketing',
                prevPath: '/distribution/distribution_grade',
                moduleName: 'distribution'
            },
            component: () => import('@/views/marketing/distribution/distribution_grade_edit.vue')
        }, {
            path: 'distribution/setting',
            name: 'setting',
            meta: {
                title: '分销设置',
                parentPath: '/marketing',
                moduleName: 'distribution',
                icon: 'icon_fenxiao_set',
                
            },
            children: [
                {
                    path: '/distribution/base_setting',
                    name: 'base_setting',
                    meta: {
                        title: '基础设置',
                        parentPath: '/marketing',
                        moduleName: 'distribution',
                        permission: ['view']
                    },
                    component: () => import('@/views/marketing/distribution/setting/base_setting.vue')
                }, {
                    path: '/distribution/result_setting',
                    name: 'result_setting',
                    meta: {
                        title: '结算设置',
                        parentPath: '/marketing',
                        moduleName: 'distribution',
                        permission: ['view']
                    },
                    component: () => import('@/views/marketing/distribution/setting/result_setting.vue')
                }
            ],
            component: Blank
        }
        ]
    },
    {
        path: '/recharge',
        name: 'recharge',
        meta: {
            title: '充值概览',
            hidden: true,
            moduleName: 'recharge'
        },
        redirect: '/recharge/survey',
        component: Main,
        children: [
            {
                path: '/recharge/survey',
                name: 'survey',
                meta: {
                    title: '充值概览',
                    parentPath: '/marketing',
                    moduleName: 'recharge',
                    icon: 'icon_pintuan_data',
                    permission: ['view']
                },
                component: () => import('@/views/marketing/recharge/survey.vue')
            }, {
                path: '/recharge/rule',
                name: 'rule',
                meta: {
                    title: '充值规则',
                    parentPath: '/marketing',
                    moduleName: 'recharge',
                    icon: 'icon_caiwu_yue',
                    permission: ['view']
                },
                component: () => import('@/views/marketing/recharge/rule.vue')
            }, {
                path: '/recharge/record',
                name: 'record',
                meta: {
                    title: '充值记录',
                    parentPath: '/marketing',
                    moduleName: 'recharge',
                    icon: 'icon_order_guanli',
                    permission: ['view']
                },
                component: () => import('@/views/marketing/recharge/record.vue')
            }
        ]
    },
    // ----日历签到
    {
        path: '/calendar',
        name: 'calendar',
        meta: {
            title: '签到概览',
            hidden: true,
            moduleName: 'calendar'
        },
        redirect: '/calendar/survey',
        component: Main,
        children: [
            {
                path: '/calendar/survey',
                name: 'survey',
                meta: {
                    title: '签到概览',
                    parentPath: '/marketing',
                    moduleName: 'calendar',
                    icon: 'icon_coupons_data',
                    permission: ['view']
                },
                component: () => import('@/views/marketing/calendar/survey.vue')
            }, {
                path: '/calendar/rule',
                name: 'rule',
                meta: {
                    title: '签到规则',
                    parentPath: '/marketing',
                    moduleName: 'calendar',
                    icon: 'icon_qiandao_guize',
                    permission: ['view']
                },
                component: () => import('@/views/marketing/calendar/rule.vue')
            }, {
                path: '/calendar/record',
                name: 'record',
                meta: {
                    title: '签到记录',
                    parentPath: '/marketing',
                    moduleName: 'calendar',
                    icon: 'icon_qiandao_jilu',
                    permission: ['view']
                },
                component: () => import('@/views/marketing/calendar/record.vue')
            }
        ]
    },
    // ----小票打印
    {
        path: '/print',
        name: 'print',
        meta: {
            title: '打印机管理',
            hidden: true,
            moduleName: 'print'
        },
        redirect: '/print/list',
        component: Main,
        children: [
            {
                path: '/print/list',
                name: 'print',
                meta: {
                    title: '打印机管理',
                    parentPath: '/marketing',
                    moduleName: 'print',
                    icon: 'icon_xpdy_dyjgl',
                    permission: ['view']
                },
                component: () => import('@/views/marketing/print/list.vue')
            }, {
                path: '/print/edit_print',
                name: 'edit_print',
                meta: {
                    hidden: true,
                    title: '打印机',
                    parentPath: '/marketing',
                    moduleName: 'print',
                    prevPath: '/print/list',
                    permission: ['view']
                },
                component: () => import('@/views/marketing/print/edit_print.vue')
            }, {
                path: '/print/template',
                name: 'template',
                meta: {
                    title: '模板管理',
                    parentPath: '/marketing',
                    icon: 'icon_xpdy_mbgl',
                    moduleName: 'print',
                    permission: ['view']
                },
                component: () => import('@/views/marketing/print/template.vue')
            }, {
                path: '/print/edit_template',
                name: 'edit_template',
                meta: {
                    hidden: true,
                    title: '模板管理',
                    parentPath: '/marketing',
                    moduleName: 'print',
                    prevPath: '/print/template',
                    icon: 'icon_qiandao_jilu',
                    permission: ['view']
                },
                component: () => import('@/views/marketing/print/edit_template.vue')
            }
        ]
    },

    // 快递助手
    {
        path: '/express',
        name: 'express',
        meta: {
            title: '快递助手',
            hidden: true,
            moduleName: 'express'
        },
        redirect: '/express/batch',
        component: Main,
        children: [
            {
                path: '/express/batch',
                name: 'express',
                meta: {
                    title: '批量打印',
                    parentPath: '/marketing',
                    moduleName: 'express',
                    icon: 'icon_kdzs_pldy',
                    permission: ['view']
                },
                component: () => import('@/views/marketing/express/batch.vue')
            }, {
                path: '/express/sender',
                name: 'edit_express',
                meta: {
                    title: '发件人模版',
                    parentPath: '/marketing',
                    moduleName: 'express',
                    icon: 'icon_kdzs_fjrmb',
                    permission: ['view']
                },
                component: () => import('@/views/marketing/express/sender.vue')
            }, {
                path: '/express/sender_edit',
                name: 'sender_edit',
                meta: {
                    hidden: true,
                    title: '编辑发件人模版',
                    parentPath: '/marketing',
                    moduleName: 'express',
                    prevPath: '/express/sender',
                    permission: ['view']
                },
                component: () => import('@/views/marketing/express/sender_edit.vue')
            }, {
                path: '/express/face_sheet',
                name: 'face_sheet',
                meta: {
                    title: '面单模版',
                    parentPath: '/marketing',
                    moduleName: 'express',
                    icon: 'icon_kdzs_mdmb',
                    permission: ['view']
                },
                component: () => import('@/views/marketing/express/face_sheet.vue')
            }, {
                path: '/express/face_sheet_edit',
                name: 'face_sheet_edit',
                meta: {
                    hidden: true,
                    title: '电子面单模版编辑',
                    parentPath: '/marketing',
                    moduleName: 'express',
                    prevPath: '/express/face_sheet',
                    permission: ['view']
                },
                component: () => import('@/views/marketing/express/face_sheet_edit.vue')
            }, {
                path: '/express/face_sheet_set',
                name: 'face_sheet_set',
                meta: {
                    title: '面单设置',
                    parentPath: '/marketing',
                    moduleName: 'express',
                    icon: 'icon_kdzs_mdsz',
                    permission: ['view']
                },
                component: () => import('@/views/marketing/express/face_sheet_set.vue')
            }
        ]
    },
    // 自提门店
    {
        path: '/selffetch',
        name: 'selffetch',
        meta: {
            title: '自提门店',
            hidden: true,
            moduleName: 'selffetch'
        },
        redirect: '/selffetch/selffetch_shop',
        component: Main,
        children: [
            {
                path: '/selffetch/selffetch_order',
                name: 'selffetch_order',
                meta: {
                    title: '核销订单',
                    parentPath: '/marketing',
                    moduleName: 'selffetch',
                    icon: 'icon_hexiao_order',
                    permission: ['view']
                },
                component: () => import('@/views/marketing/selffetch/order.vue')
            },
            {
                path: '/selffetch/selffetch_shop',
                name: 'selffetch_shop',
                meta: {
                    title: '自提门店',
                    parentPath: '/marketing',
                    moduleName: 'selffetch',
                    icon: 'icon_ziti_store',
                    permission: ['view']
                },
                component: () => import('@/views/marketing/selffetch/shop.vue')
            },
            {
                path: '/selffetch/selffetch_shop_edit',
                name: 'selffetch_shop_edit',
                meta: {
                    title: '编辑自提门店',
                    hidden: true,
                    parentPath: '/marketing',
                    prevPath: '/selffetch/selffetch_shop',
                    moduleName: 'selffetch'
                },
                component: () => import('@/views/marketing/selffetch/shop_edit.vue')
            },
            {
                path: '/selffetch/selffetch_verifier',
                name: 'selffetch_verifier',
                meta: {
                    title: '核销员',
                    parentPath: '/marketing',
                    moduleName: 'selffetch',
                    icon: 'icon_hexiao_member',
                    permission: ['view']
                },
                component: () => import('@/views/marketing/selffetch/verifier.vue')
            },
            {
                path: '/selffetch/selffetch_verifier_edit',
                name: 'selffetch_verifier_edit',
                meta: {
                    title: '编辑核销员',
                    hidden: true,
                    parentPath: '/marketing',
                    prevPath: '/selffetch/selffetch_verifier',
                    moduleName: 'selffetch'
                },
                component: () => import('@/views/marketing/selffetch/verifier_edit.vue')
            },
        ]
    },
    // 消息通知
    {
        path: '/sms',
        name: 'sms',
        meta: {
            title: '通知买家',
            hidden: true,
            moduleName: 'sms'
        },
        redirect: '/sms/buyers',
        component: Main,
        children: [
            {
                path: '/sms/buyers',
                name: 'sms_buyers',
                meta: {
                    title: '通知买家',
                    parentPath: '/marketing',
                    moduleName: 'sms',
                    icon: 'icon_notice_buyer',
                    
                },
                component: Blank,
                redirect: '/sms/buyers/buyers',
                children: [{
                    path: '/sms/buyers/buyers',
                    name: 'sms_buyers',
                    meta: {
                        title: '业务通知',
                        parentPath: '/marketing',
                        moduleName: 'sms',
                        permission: ['view']
                    },
                    component: () => import('@/views/marketing/sms/buyers/buyers.vue')
                },
                {
                    path: '/sms/buyers/setting',
                    name: 'setting',
                    meta: {
                        hidden: true,
                        title: '设置',
                        parentPath: '/marketing',
                        moduleName: 'sms'
                    },
                    component: () => import('@/views/marketing/sms/buyers/setting.vue')
                },
                {
                    path: '/sms/buyers/business',
                    name: 'business',
                    meta: {
                        title: '验证码',
                        parentPath: '/marketing',
                        moduleName: 'sms',
                        permission: ['view']
                    },
                    component: () => import('@/views/marketing/sms/buyers/business.vue')
                }
                ],
            }, {
                path: '/sms/seller',
                name: 'seller',
                meta: {
                    title: '卖家通知',
                    parentPath: '/marketing',
                    moduleName: 'sms',
                    icon: 'icon_notice_seller',
                    permission: ['view']
                },
                component: () => import('@/views/marketing/sms/seller.vue')
            },
            {
                path: '/sms/sms',
                name: 'sms',
                meta: {
                    title: '短信设置',
                    parentPath: '/marketing',
                    moduleName: 'sms',
                    icon: 'icon_notice_mail',
                    permission: ['view']
                },
                component: () => import('@/views/marketing/sms/sms.vue')
            },
            {
                path: '/sms/sms_edit',
                name: 'sms_edit',
                meta: {
                    hidden: true,
                    title: '短信设置详情',
                    parentPath: '/marketing',
                    prevPath: '/sms/sms',
                    moduleName: 'sms',
                },
                component: () => import('@/views/marketing/sms/sms_edit.vue')
            }
        ]
    },
    {
        path: '/notice',
        name: 'notice',
        meta: {
            hidden: true,
            title: '商城公告',
            moduleName: 'notice'
        },
        redirect: '/notice',
        component: Main,
        children: [{
            path: '/notice/lists',
            name: 'notice',
            meta: {
                title: '公告管理',
                parentPath: '/marketing',
                moduleName: 'notice',
                icon: 'icon_notice',
                permission: ['view']
            },
            component: () => import('@/views/marketing/notice/lists.vue')
        }, {
            path: '/notice/notice_edit',
            name: 'notice_edit',
            meta: {
                hidden: true,
                title: '商城公告',
                parentPath: '/notice',
                prevPath: '/notice/lists',
                moduleName: 'notice',
            },
            component: () => import('@/views/marketing/notice/notice_edit.vue')
        }]
    },
    {
        path: '/service',
        name: 'service',
        meta: {
            hidden: true,
            title: '在线客服',
            moduleName: 'service'
        },
        redirect: '/service/setting',
        component: Main,
        children: [
            {
                path: '/service/setting',
                name: 'service_setting',
                meta: {
                    title: '在线客服',
                    parentPath: '/marketing',
                    moduleName: 'service',
                    icon: 'icon_kefu_comments',
                    permission: ['view']
                },
                component: () => import('@/views/marketing/service/setting.vue')
            },
            {
                path: '/service/lists',
                name: 'service_lists',
                meta: {
                    title: '客服列表',
                    parentPath: '/marketing',
                    moduleName: 'service',
                    icon: 'icon_user_biaoqian',
                    permission: ['view']
                },
                component: () => import('@/views/marketing/service/lists.vue')
            },
            {
                path: '/service/edit',
                name: 'service_edit',
                meta: {
                    hidden: true,
                    title: '客服列表',
                    parentPath: '/marketing',
                    prevPath: '/service/lists',
                    moduleName: 'service',
                    permission: ['view'],
                },
                component: () => import('@/views/marketing/service/edit.vue')
            },
            {
                path: '/service/speech_lists',
                name: 'service_speech_lists',
                meta: {
                    title: '客服话术',
                    parentPath: '/marketing',
                    moduleName: 'service',
                    icon: 'icon_notice_buyer',
                    permission: ['view']
                },
                component: () => import('@/views/marketing/service/speech_lists.vue')
            },
        ]
    },
    // 砍价活动
    {
        path: '/bargain',
        name: 'bargain',
        meta: {
            hidden: true,
            title: '砍价活动',
            moduleName: 'bargain'
        },
        redirect: '/bargain/lists',
        component: Main,
        children: [{
            path: '/bargain/lists',
            name: 'bargain',
            meta: {
                title: '砍价活动',
                parentPath: '/marketing',
                moduleName: 'bargain',
                icon: 'icon_kanjia',
                permission: ['view']
            },
            component: () => import('@/views/marketing/bargain/lists.vue')
        }, {
            path: '/bargain/bargain_edit',
            name: 'bargain_edit',
            meta: {
                hidden: true,
                title: '砍价活动',
                parentPath: '/marketing',
                prevPath: '/bargain/lists',
                moduleName: 'bargain'
            },
            component: () => import('@/views/marketing/bargain/bargain_edit.vue')
        },{
            path: '/bargain/bargain_record',
            name: 'bargain_record',
            meta: {
                title: '砍价记录',
                parentPath: '/marketing',
                icon: 'icon_order_guanli',
                moduleName: 'bargain',
                permission: ['view']
            },
            component: () => import('@/views/marketing/bargain/bargain_record.vue')
        }]
    },
    // 拼团活动
    {
        path: '/combination',
        name: 'combination',
        meta: {
            hidden: true,
            title: '拼团活动',
            moduleName: 'combination'
        },
        redirect: '/combination/lists',
        component: Main,
        children: [{
            path: '/combination/survey',
            name: 'combination',
            meta: {
                hidden: true,
                title: '拼团概览',
                parentPath: '/marketing',
                moduleName: 'combination',
                icon: 'icon_pintuan_data',
                permission: ['view']
            },
            component: () => import('@/views/marketing/combination/survey.vue')
        },{
            path: '/combination/lists',
            name: 'combination',
            meta: {
                title: '拼团活动',
                parentPath: '/marketing',
                moduleName: 'combination',
                icon: 'icon_pintuan2',
                permission: ['view']
            },
            component: () => import('@/views/marketing/combination/lists.vue')
        }, {
            path: '/combination/edit',
            name: 'edit',
            meta: {
                hidden: true,
                title: '拼团活动',
                prevPath: '/combination/lists',
                moduleName: 'combination'
            },
            component: () => import('@/views/marketing/combination/edit.vue')
        }, {
            path: '/combination/record',
            name: 'record',
            meta: {
                title: '拼团记录',
                icon: 'icon_order_guanli',
                parentPath: '/marketing',
                moduleName: 'combination'
            },
            component: () => import('@/views/marketing/combination/record.vue')
        }]
    },
    // 限时秒杀
    {
        path: '/seckill',
        name: 'seckill',
        meta: {
            hidden: true,
            title: '限时秒杀',
            moduleName: 'seckill'
        },
        redirect: '/seckill/lists',
        component: Main,
        children: [{
            path: '/seckill/lists',
            name: 'seckill',
            meta: {
                title: '秒杀活动',
                parentPath: '/marketing',
                moduleName: 'seckill',
                icon: 'Field-time',
                permission: ['view']
            },
            component: () => import('@/views/marketing/seckill/lists.vue')
        }, {
            path: '/seckill/edit',
            name: 'seckill_edit',
            meta: {
                hidden: true,
                title: '秒杀活动',
                parentPath: '/marketing',
                prevPath: '/seckill/lists',
                moduleName: 'seckill'
            },
            component: () => import('@/views/marketing/seckill/edit.vue')
        }]
    },
	// 直播间
	{
	    path: '/live_broadcast',
	    name: 'live_broadcast',
	    meta: {
	        hidden: true,
	        title: '直播间',
	        moduleName: 'live_broadcast'
	    },
	    redirect: '/live_broadcast/lists',
	    component: Main,
	    children: [{
	        path: '/live_broadcast/lists',
	        name: 'live_broadcast_list',
	        meta: {
	            title: '直播间',
	            parentPath: '/marketing',
	            moduleName: 'live_broadcast',
	            icon: 'icon_xcxzb_zb',
	            permission: ['view']
	        },
	        component: () => import('@/views/marketing/live_broadcast/lists.vue')
	    }, {
	        path: '/live_broadcast/edit',
	        name: 'live_broadcast_edit',
	        meta: {
	            hidden: true,
	            title: '创建直播间',
	            parentPath: '/marketing',
	            prevPath: '/live_broadcast/lists',
	            moduleName: 'live_broadcast'
	        },
	        component: () => import('@/views/marketing/live_broadcast/edit.vue')
	    },{
	        path: '/live_broadcast/goods',
	        name: 'live_broadcast_goods',
	        meta: {
	            title: '直播商品',
	            parentPath: '/marketing',
	            icon: 'icon_pintuan',
	            moduleName: 'live_broadcast',
	            permission: ['view']
	        },
	        component: () => import('@/views/marketing/live_broadcast/goods.vue')
	    },{
	        path: '/live_broadcast/add_broadcast_goods',
	        name: 'live_broadcast_goods_add',
	        meta: {
				hidden: true,
	            title: '添加直播商品',
	            parentPath: '/marketing',
	            icon: '',
	            moduleName: 'live_broadcast',
	            permission: ['view']
	        },
	        component: () => import('@/views/marketing/live_broadcast/add_broadcast_goods.vue')
	    }]
	},
	
	// 幸运抽奖
	{
	    path: '/lucky_draw',
	    name: 'lucky_draw',
	    meta: {
	        hidden: true,
	        title: '幸运抽奖',
	        moduleName: 'lucky_draw'
	    },
	    redirect: '/lucky_draw/index',
	    component: Main,
	    children: [{
	        path: '/lucky_draw/index',
	        name: 'lucky_draw_index',
	        meta: {
	            title: '幸运抽奖',
	            parentPath: '/marketing',
	            moduleName: 'lucky_draw',
	            icon: 'icon_xycj_cj',
	            permission: ['view']
	        },
	        component: () => import('@/views/marketing/lucky_draw/index.vue')
	    }, {
	        path: '/lucky_draw/edit',
	        name: 'lucky_draw_edit',
	        meta: {
	            hidden: true,
	            title: '编辑幸运抽奖',
	            parentPath: '/marketing',
	            prevPath: '/lucky_draw/index',
	            moduleName: 'lucky_draw'
	        },
	        component: () => import('@/views/marketing/lucky_draw/edit.vue')
	    }, {
	        path: '/lucky_draw/log',
	        name: 'lucky_draw_log',
	        meta: {
	            hidden: true,
	            title: '幸运记录',
	            parentPath: '/marketing',
	            prevPath: '/lucky_draw/log',
	            moduleName: 'lucky_draw'
	        },
	        component: () => import('@/views/marketing/lucky_draw/log.vue')
	    }]
	},
    	// 虚拟评价
	{
	    path: '/evaluation',
	    name: 'evaluation',
	    meta: {
	        hidden: true,
	        title: '虚拟评价',
	        moduleName: 'evaluation'
	    },
	    redirect: '/evaluation/index',
	    component: Main,
	    children: [{
	        path: '/evaluation/index',
	        name: 'evaluation',
	        meta: {
	            title: '虚拟评价',
	            parentPath: '/marketing',
	            moduleName: 'evaluation',
	            icon: 'icon_pingjia',
	            permission: ['view']
	        },
	        component: () => import('@/views/marketing/evaluation/index.vue')
	    }, {
	        path: '/evaluation/add',
	        name: 'evaluation_add',
	        meta: {
	            hidden: true,
	            title: '添加评价',
	            parentPath: '/marketing',
	            prevPath: '/evaluation/index',
	            moduleName: 'evaluation'
	        },
	        component: () => import('@/views/marketing/evaluation/add.vue')
	    }]
	},
	// 文章资讯
	{
	    path: '/article',
	    name: 'article',
	    meta: {
	        hidden: true,
	        title: '文章资讯',
	        moduleName: 'article'
	    },
	    redirect: '/article',
	    component: Main,
	    children: [{
	        path: '/article/lists',
	        name: 'article',
	        meta: {
	            title: '资讯管理',
	            parentPath: '/marketing',
	            moduleName: 'article',
	            icon: 'icon_notice',
	            permission: ['view']
	        },
	        component: () => import('@/views/marketing/article/lists.vue')
	    }, {
	        path: '/article/article_edit',
	        name: 'article_edit',
	        meta: {
	            hidden: true,
	            title: '资讯管理',
	            parentPath: '/article',
	            prevPath: '/article/lists',
	            moduleName: 'article',
	        },
	        component: () => import('@/views/marketing/article/article_edit.vue')
	    }, {
			path: '/article/category_lists',
			name: 'category_lists',
			meta: {
				title: '资讯分类',
				parentPath: '/marketing',
				moduleName: 'article',
				icon: 'icon_set_weihu',
				permission: ['view']
			},
			component: () => import('@/views/marketing/article/category_lists.vue')
		}]
	},
]

export default routes