const cn :any = {
  global:{
    login:'login',
    LeavingToSaveChanges:"Are you sure to leave this page?  The system may not save the changes you make.  ",
    cancel:"cancel",
    confirm:"confirm",
    goBack:"go back",
  },
  login:{
    login:'login',
    usernamePlaceholder:"Please enter your account number",
    loginPlaceholder:'Please enter your password',
    rememberAccount:'Remember account',
  },
  shop:{
    StoreInformation:"Store Information",
    TodayData:"Today's data",
  },
  menue:{
    index:"Index",
    shop:{
      shop:"Shop",
      shopIndex:"Shop Index",
      shopCategory:"Shop Category",
      shopGoodsDetail:"Shop Goods",
      shopCart:"Cart",
      personalCenter:"Personal Center",
      bottomNavigation:"Bottom Nav",
      microPage:"Micro Page",
      pageManage:"Page Manage",
      pageTemplate:"Page Templates",
      mallStyle:"Mall Style",
      materialCenter:"Material center",
      pcShop:"PC Shop",
      indexDecoration:"Index Decoration",
      advertising:"Advertising",
    },
    goods:{
      goods:"Goods",
      category:"Category",
      brand:"Crand",
      goodsUnit:"Goods Unit",
      supplier:"Supplier",
      supplierManager:"Supplier",
      supplierCategory:"Category",
      evaluation:"Evaluation",
    },
    order:{
      order:"Order",
      orderDetail:"Order Detail",
      aftermarket:"Aftermarket",
      aftermarkeDetailt:"Aftermarket Detailt",
    },
    user:{
      user:"User",
      general:"General",
      manage:"Manage",
      grade:"Grade",
      label:"Label",
    },
    marketing:{
      marketing:"Marketing",
      application:"Application",
    },
    finance:{
      finance:"Finance",
      withdrawCash:"Withdraw Cash",
      balance:"Balance",
      score:"Score",
      brokerage:"Brokerage",
    },
    data:{
      data:"Data",
      flowAnalysis:'Flow',
      userAnalysis:'User',
      TransactionAnalysis:'Transaction',
      goods:"Goods",
      Ranking:"Ranking",
    },
    channel:{
      channel:"Channel",
    },
    settings:{
      settings:"Settings"
    },
  },
};

export default cn;