const setcache = (key, value) => { // 设置缓存
    let getsetcache = ''
    if (key !== '' && key !== null && key !== undefined) {
        // 设置缓存
        localStorage.setItem(key, value)
            // 获取设置结果
        getsetcache = localStorage.getItem(key)
    }
    return getsetcache
}
const timestamp = (time) => { // 时间戳
        const date = time.length == 10 ? new Date(time * 1000) : new Date(time) // 时间戳为10位需*1000，时间戳为13位的话不需乘1000
        const Y = `${date.getFullYear()}-`
        const M = `${date.getMonth() + 1 < 10 ? `0${date.getMonth() + 1}` : date.getMonth() + 1}-`
        const D = `${date.getDate()} `
        const h = `${date.getHours()}:`
        const m = date.getMinutes() < 10 ? `0${date.getMinutes()}:` : `${date.getMinutes()}:`
        const s = date.getSeconds()
        return Y + M + D + h + m + s
    }
const getcache = (key) => { // 读取缓存
    let getgetcache = ''
    if (key !== '' && key !== null && key !== undefined) {
        getgetcache = localStorage.getItem(key)
    }
    return getgetcache
}

const delcache = (key) => { // 删除缓存
    if (key !== '' && key !== null && key !== undefined) {
        localStorage.removeItem(key)
    }
}
const NumberMul = (arg1, arg2) => { //浮点型乘法
    var m = 0;
    var s1 = arg1.toString();
    var s2 = arg2.toString();
    try {
        m += s1.split(".")[1].length;
    } catch (e) {}
    try {
        m += s2.split(".")[1].length;
    } catch (e) {}
    return Number(s1.replace(".", "")) * Number(s2.replace(".", "")) / Math.pow(10, m);
}
const NumberAdd = (arg1, arg2) => { //解决浮点型加法
    var r1, r2, m, n;
    try {
        r1 = arg1.toString().split(".")[1].length
    } catch (e) {
        r1 = 0
    }
    try {
        r2 = arg2.toString().split(".")[1].length
    } catch (e) {
        r2 = 0
    }
    m = Math.pow(10, Math.max(r1, r2))
    n = (r1 >= r2) ? r1 : r2;
    return ((arg1 * m + arg2 * m) / m).toFixed(n);
}
const NumberSub = (arg1, arg2) => { //解决浮点型减法
    var re1, re2, m, n;
    try {
        re1 = arg1.toString().split(".")[1].length;
    } catch (e) {
        re1 = 0;
    }
    try {
        re2 = arg2.toString().split(".")[1].length;
    } catch (e) {
        re2 = 0;
    }
    m = Math.pow(10, Math.max(re1, re2));
    n = (re1 >= re2) ? re1 : re2;
    return ((arg1 * m - arg2 * m) / m).toFixed(n);
}

const NumberDiv = (arg1, arg2, digit) => { //解决除法浮点型小数
    var t1 = 0,
        t2 = 0,
        r1, r2;
    try { t1 = arg1.toString().split(".")[1].length } catch (e) {}
    try { t2 = arg2.toString().split(".")[1].length } catch (e) {}
    r1 = Number(arg1.toString().replace(".", ""))
    r2 = Number(arg2.toString().replace(".", ""))
        //获取小数点后的计算值
    var result = ((r1 / r2) * Math.pow(10, t2 - t1)).toString()
    var result2 = result.split(".")[1];
    result2 = result2.substring(0, digit > result2.length ? result2.length : digit);

    return Number(result.split(".")[0] + "." + result2);
}
export {
    setcache,
    getcache,
    delcache,
    NumberDiv,
    NumberSub,
    NumberAdd,
    NumberMul,
    timestamp
}